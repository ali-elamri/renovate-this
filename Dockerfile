# renovate: datasource=github-tags depName=nodejs/node versioning=node
ARG NODE_VERSION=12
ARG NODE_BUILDER_IMAGE=registry.gitlab.com/relex-work/base-images/node-builder:$NODE_VERSION
ARG NODE_RUNNER_IMAGE=registry.gitlab.com/relex-work/base-images/node-runner:$NODE_VERSION

FROM $NODE_BUILDER_IMAGE as dependencies
# This first stage will install dependencies
# It is used by the runner in development mode to provide a stable development environment

ARG AUTH_TOKEN=''
ARG NODE_ENV=production
ARG VERSION=development

ENV AUTH_TOKEN $AUTH_TOKEN

COPY .npmrc package.json package-lock.json $APP_PATH/
RUN npm ci --production=false


FROM dependencies as builder
# This stage will copy in the required files for building the app and run the build

ARG NODE_ENV=production
ARG VERSION=development

COPY tsconfig.json .eslintrc.js prettier.config.js $APP_PATH/
COPY schemas $APP_PATH/schemas/
COPY tasks/types.ts $APP_PATH/tasks/

# The COPY command copies as root user, while the RUN command is executed as the user specified in USER, which is "app"
# in our case. If we copy the src folder, the folder is owned by root and when we then run the type generation script,
# the script cannot write to the generated types folder. So we create the folder where the generated types live first
# with the RUN command, guaranteeing that the type genration script has access to it later.
RUN mkdir -p $APP_PATH/src/types/generated

COPY src $APP_PATH/src/

RUN npm run build --silent --no-progress && \
  echo "$VERSION" > dist/version.txt && \
  npm prune --production=true --silent --no-progress


FROM $NODE_RUNNER_IMAGE
# This stage is the minimal runtime image, that only contains the fully built app and dependencies required for running it.

ENV NODE_ENV production
ENV ZENOPT_SERVICE yearlyleavecalculator

COPY --from=builder $APP_PATH/package.json $APP_PATH/package-lock.json $APP_PATH/
COPY --from=builder $APP_PATH/node_modules $APP_PATH/node_modules/
COPY --from=builder $APP_PATH/dist $APP_PATH/dist/

CMD ["node", "dist/app.js"]